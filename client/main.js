import React from 'react';
import ReactDOM from 'react-dom';
import {Meteor} from 'meteor/meteor';

const players = [{
    _id: '1',
    name: 'Shaylee',
    score: 7
}, {
    _id: '2',
    name: 'Johnny',
    score: 2
}, {
    _id: '3',
    name: 'Macady',
    score: 7
},{
    _id: '4',
    name: 'Jerzee',
    score: 9
}];

const renderPlayers = function (playersList) {

    return playersList.map(function (playerInfo) {
        return <p key={playerInfo._id}>{playerInfo.name} has {playerInfo.score} point(s)</p>;
    });
};

Meteor.startup(function () {
    let title = 'My Stuff in React';
    let name = 'Johnny';
    let jsx = (
        <div>
            <h1>{title}</h1>
            <p>This is {name}!</p>
            <p> This is my second P.</p>
            {renderPlayers(players)}
        </div>);
    ReactDOM.render(jsx, document.getElementById('app'));
});